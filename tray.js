/* $Id: tray.js,v 1.1 2010/07/12 19:25:01 arshadchummun Exp $ */
Drupal.behaviors.tray = function (context){
    
  $('.tray-item-handle:not(.tray-processed)', context).click(function(e){
    e.preventDefault();
    
    var $this = $(this);
    
    $('.tray-item-active').removeClass('tray-item-active');
    $this.parent('li').toggleClass('tray-item-active');
    
    $('li.tray-item').each(function(){
        
        if(!$(this).hasClass('tray-item-active')){
            $(this).find('.tray-item-block').hide();
        }
    });
    
    $this.next('.tray-item-block').slideToggle(100);
    
  }).addClass('tray-processed');
  
  $('.tray-item-minimize:not(.tray-processed)', context).click(function(e){
        e.preventDefault();
        $('.tray-item-active').find('.tray-item-block').slideToggle(100);
  }).addClass('tray-processed');
  
  $('.tray-item-close:not(.tray-processed)', context).click(function(e){
        e.preventDefault();
        $('.tray-item-active').find('.tray-item-block').fadeOut(250);
        $('.tray-item-active').removeClass('tray-item-active');
  }).addClass('tray-processed');
  
      
}